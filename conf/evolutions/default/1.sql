# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table api_key (
  id                            bigint auto_increment not null,
  api_key                       varchar(255),
  constraint pk_api_key primary key (id)
);

create table country (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_country primary key (id)
);

create table genre (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_genre primary key (id)
);

create table movie (
  id                            bigint auto_increment not null,
  title                         varchar(255),
  original_title                varchar(255),
  year                          integer not null,
  duration                      varchar(255),
  country_id                    bigint,
  synopsis                      varchar(15000),
  awards                        varchar(5000),
  cover_url                     varchar(255),
  constraint pk_movie primary key (id)
);

create table movie_producer (
  movie_id                      bigint not null,
  producer_id                   bigint not null,
  constraint pk_movie_producer primary key (movie_id,producer_id)
);

create table movie_genre (
  movie_id                      bigint not null,
  genre_id                      bigint not null,
  constraint pk_movie_genre primary key (movie_id,genre_id)
);

create table movie_subgenre (
  movie_id                      bigint not null,
  subgenre_id                   bigint not null,
  constraint pk_movie_subgenre primary key (movie_id,subgenre_id)
);

create table performance (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_performance primary key (id)
);

create table person (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_person primary key (id)
);

create table producer (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_producer primary key (id)
);

create table subgenre (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_subgenre primary key (id)
);

create table teammember (
  id                            bigint auto_increment not null,
  movie_id                      bigint,
  person_id                     bigint,
  performance_id                bigint,
  constraint pk_teammember primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  email                         varchar(255),
  pass                          varchar(255),
  api_key_id                    bigint,
  constraint uq_user_api_key_id unique (api_key_id),
  constraint pk_user primary key (id)
);

create table warehouse (
  id                            bigint auto_increment not null,
  movie_id                      bigint,
  user_id                       bigint,
  favorite                      tinyint(1) default 0 not null,
  viewed                        tinyint(1) default 0 not null,
  pending                       tinyint(1) default 0 not null,
  constraint pk_warehouse primary key (id)
);

alter table movie add constraint fk_movie_country_id foreign key (country_id) references country (id) on delete restrict on update restrict;
create index ix_movie_country_id on movie (country_id);

alter table movie_producer add constraint fk_movie_producer_movie foreign key (movie_id) references movie (id) on delete restrict on update restrict;
create index ix_movie_producer_movie on movie_producer (movie_id);

alter table movie_producer add constraint fk_movie_producer_producer foreign key (producer_id) references producer (id) on delete restrict on update restrict;
create index ix_movie_producer_producer on movie_producer (producer_id);

alter table movie_genre add constraint fk_movie_genre_movie foreign key (movie_id) references movie (id) on delete restrict on update restrict;
create index ix_movie_genre_movie on movie_genre (movie_id);

alter table movie_genre add constraint fk_movie_genre_genre foreign key (genre_id) references genre (id) on delete restrict on update restrict;
create index ix_movie_genre_genre on movie_genre (genre_id);

alter table movie_subgenre add constraint fk_movie_subgenre_movie foreign key (movie_id) references movie (id) on delete restrict on update restrict;
create index ix_movie_subgenre_movie on movie_subgenre (movie_id);

alter table movie_subgenre add constraint fk_movie_subgenre_subgenre foreign key (subgenre_id) references subgenre (id) on delete restrict on update restrict;
create index ix_movie_subgenre_subgenre on movie_subgenre (subgenre_id);

alter table teammember add constraint fk_teammember_movie_id foreign key (movie_id) references movie (id) on delete restrict on update restrict;
create index ix_teammember_movie_id on teammember (movie_id);

alter table teammember add constraint fk_teammember_person_id foreign key (person_id) references person (id) on delete restrict on update restrict;
create index ix_teammember_person_id on teammember (person_id);

alter table teammember add constraint fk_teammember_performance_id foreign key (performance_id) references performance (id) on delete restrict on update restrict;
create index ix_teammember_performance_id on teammember (performance_id);

alter table user add constraint fk_user_api_key_id foreign key (api_key_id) references api_key (id) on delete restrict on update restrict;

alter table warehouse add constraint fk_warehouse_movie_id foreign key (movie_id) references movie (id) on delete restrict on update restrict;
create index ix_warehouse_movie_id on warehouse (movie_id);

alter table warehouse add constraint fk_warehouse_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_warehouse_user_id on warehouse (user_id);


# --- !Downs

alter table movie drop foreign key fk_movie_country_id;
drop index ix_movie_country_id on movie;

alter table movie_producer drop foreign key fk_movie_producer_movie;
drop index ix_movie_producer_movie on movie_producer;

alter table movie_producer drop foreign key fk_movie_producer_producer;
drop index ix_movie_producer_producer on movie_producer;

alter table movie_genre drop foreign key fk_movie_genre_movie;
drop index ix_movie_genre_movie on movie_genre;

alter table movie_genre drop foreign key fk_movie_genre_genre;
drop index ix_movie_genre_genre on movie_genre;

alter table movie_subgenre drop foreign key fk_movie_subgenre_movie;
drop index ix_movie_subgenre_movie on movie_subgenre;

alter table movie_subgenre drop foreign key fk_movie_subgenre_subgenre;
drop index ix_movie_subgenre_subgenre on movie_subgenre;

alter table teammember drop foreign key fk_teammember_movie_id;
drop index ix_teammember_movie_id on teammember;

alter table teammember drop foreign key fk_teammember_person_id;
drop index ix_teammember_person_id on teammember;

alter table teammember drop foreign key fk_teammember_performance_id;
drop index ix_teammember_performance_id on teammember;

alter table user drop foreign key fk_user_api_key_id;

alter table warehouse drop foreign key fk_warehouse_movie_id;
drop index ix_warehouse_movie_id on warehouse;

alter table warehouse drop foreign key fk_warehouse_user_id;
drop index ix_warehouse_user_id on warehouse;

drop table if exists api_key;

drop table if exists country;

drop table if exists genre;

drop table if exists movie;

drop table if exists movie_producer;

drop table if exists movie_genre;

drop table if exists movie_subgenre;

drop table if exists performance;

drop table if exists person;

drop table if exists producer;

drop table if exists subgenre;

drop table if exists teammember;

drop table if exists user;

drop table if exists warehouse;

