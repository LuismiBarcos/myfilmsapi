package validators;

import java.util.List;

import core.AppSettings;
import models.Subgenre;
import play.data.validation.Constraints;
import play.libs.F.Tuple;

public class SubgenresValidator extends Constraints.Validator<List<Subgenre>>{

	@Override
	public boolean isValid(List<Subgenre> subgenres) {
		if(subgenres == null) {
			return true;		// Subgenres not required
		}
		for (Subgenre subgenre : subgenres) {
			if(!isValid(subgenre)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Tuple<String, Object[]> getErrorMessageKey() {
		return new Tuple<String, Object[]>(AppSettings.messages.at("custom-validator-error"), new Object[] {""});
	}
	
	private boolean isValid(Subgenre subgenre) {
		if(subgenre.getName() != null) {
			return false;
		}
		return subgenre.getId() == null ? false : true;
	}
}
