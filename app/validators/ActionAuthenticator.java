package validators;

import javax.inject.Inject;

import core.AppSettings;
import models.ApiKey;
import models.User;
import play.mvc.Http;
import play.mvc.Result;
import services.ISender;

public class ActionAuthenticator extends play.mvc.Security.Authenticator{
	
	private final ISender sender;
	
	@Inject
	public ActionAuthenticator(ISender sender) {
		this.sender = sender;
	}
	
	@Override
	public String getUsername(Http.Context ctx) {
		String apiKeyHash = ctx.request().getQueryString("api_key");
		if(apiKeyHash != null) {
			ApiKey apiKey = ApiKey.findByHash(apiKeyHash);
			return apiKey == null ? null : User.findNameByApiKey(apiKey.getId());
		}
		return null;
	}
	
	@Override
	public Result onUnauthorized(Http.Context ctx) {		
		return sender.sendError(Http.Status.UNAUTHORIZED, AppSettings.messages.at("unauthorized-message"));
	}
}
