package validators;

import java.util.List;

import core.AppSettings;
import models.Producer;
import play.data.validation.Constraints;
import play.libs.F.Tuple;

public class ProducersValidator extends Constraints.Validator<List<Producer>>{

	@Override
	public boolean isValid(List<Producer> producers) {
		if(producers == null) {
			return true;
		}
		for (Producer producer : producers) {
			if(!isValid(producer)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Tuple<String, Object[]> getErrorMessageKey() {
		return new Tuple<String, Object[]>(AppSettings.messages.at("custom-validator-error"), new Object[] {""});
	}
	
	private boolean isValid(Producer producer) {
		if(producer.getName() != null) {
			return false;
		}
		return producer.getId() == null ? false : true;
	}
}
