package validators;

import java.util.List;

import core.AppSettings;
import models.TeamMember;
import play.data.validation.Constraints;
import play.libs.F.Tuple;

public class TeamMembersValidator extends Constraints.Validator<List<TeamMember>>{

	@Override
	public boolean isValid(List<TeamMember> teamMembers) {
		if(teamMembers == null) {
			return false;
		}
		for (TeamMember teamMember : teamMembers) {
			if(!isValid(teamMember)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Tuple<String, Object[]> getErrorMessageKey() {
		return new Tuple<String, Object[]>(AppSettings.messages.at("teamMember-validator-error"), new Object[] {""});
	}
	
	private boolean isValid(TeamMember teamMember) {
		if(teamMember.getPerson().getId() == null || teamMember.getPerformance().getId() == null) {
			return false;
		}
		return true;
	}
}
