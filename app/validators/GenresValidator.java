package validators;

import java.util.List;

import core.AppSettings;
import models.Genre;
import play.data.validation.Constraints;
import play.libs.F.Tuple;

public class GenresValidator extends Constraints.Validator<List<Genre>>{

	@Override
	public boolean isValid(List<Genre> genres) {
		if(genres == null) {
			return false;
		}
		for (Genre genre : genres) {
			if(!isValid(genre)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Tuple<String, Object[]> getErrorMessageKey() {
		return new Tuple<String, Object[]>(AppSettings.messages.at("custom-validator-error"), new Object[] {""});
	}
	
	private boolean isValid(Genre genre) {
		if(genre.getName() != null) {
			return false;
		}
		return genre.getId() == null ? false : true;
	}
}
