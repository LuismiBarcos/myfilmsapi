package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.Producer;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class ProducersController extends Controller {
	
	@Inject
	private FormFactory formFactory; 	
	private final ISender sender;
	
	@Inject
	public ProducersController(ISender sender) {
		this.sender = sender;
	}
	
	public Result retrieveProducers(Integer page) {
		List<Producer> producers;
		String name = request().getQueryString("name");
		if(name != null) {
			producers = Producer.searchByName(name);
			return sender.sendViews(producers, request());
		}
		producers = Producer.findPage(page).getList();
		return sender.sendViews(producers, request());
	}
	
	public Result retrieveProducer(Long producerId) {
		Producer producer = Producer.findById(producerId);
		return producer == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(producer, request());  
	}
	
	public Result createProducer() {
		Form<Producer> form = formFactory.form(Producer.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updateProducer() {
		Form<Producer> form = formFactory.form(Producer.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updateProducer();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deleteProducer(Long producerId) {
		return Producer.deleteProducer(producerId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
