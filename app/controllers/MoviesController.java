package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class MoviesController extends Controller{
	
	@Inject
	private FormFactory formFactory; 
	private final ISender sender;
	
	@Inject
	public MoviesController(ISender sender) {
		this.sender = sender;
	}
	
	public Result retrieveMovies(Integer page) {
		List<Movie> movies = Movie.findPage(page).getList();
		return sender.sendViews(movies, request());
	}
	
	public Result retrieveMovie(Long movieId) {
		Movie movie = Movie.findById(movieId);
		return movie == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(movie, request());
	}
	
	public Result searchMovies(Integer page) {
		String title = request().getQueryString("title");
		String yearString = request().getQueryString("year");
		Integer year;
		try {
			year = yearString != null ? Integer.parseInt(yearString) : 0;
		} catch(Exception ex) {
			return sender.sendError(Http.Status.BAD_REQUEST, AppSettings.messages.at("movie-year-parameter-error"));
		}
		title = title == null ? "" : title;
		return sender.sendViews(Movie.searchByFilters(title, year, page).getList(), request());
	}
	
	public Result createMovie() {
		Form<Movie> form = formFactory.form(Movie.class).bindFromRequest();
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updateMovie() {
		Form<Movie> form = formFactory.form(Movie.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updateMovie();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deleteMovie(Long movieId) {
		return Movie.deleteMovie(movieId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
