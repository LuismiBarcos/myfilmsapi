package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.User;
import models.Warehouse;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class UsersController extends Controller{
	
	@Inject
	private FormFactory formFactory;
	private final ISender sender;
	
	@Inject
	public UsersController(ISender sender) {
		this.sender = sender;
	}
	
	public Result retrieveUsers(Integer page) {
		List<User> users;
		String username = request().getQueryString("username");
		if(username != null) {
			users = User.searchByUsername(username);
			return sender.sendViews(users, request());
		}
		users = User.findPage(page).getList();
		return sender.sendViews(users, request());
	}
	
	public Result retrieveUser(Long userId) {
		User user = User.findById(userId);
		return user == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(user, request()); 
	}
	
	public Result createUser() {
		Form<User> form = formFactory.form(User.class).bindFromRequest();
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updateUser() {
		Form<User> form = formFactory.form(User.class).bindFromRequest();
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updateUser();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deleteUser(Long userId) {
		return User.deleteUser(userId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
	
	public Result setWarehouse() {
		Form<Warehouse> form = formFactory.form(Warehouse.class).bindFromRequest();
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().setWarehouse();
		switch(httpCode) {
			case Http.Status.CREATED:
				return Results.created();
			case Http.Status.OK:
				return ok();
			default:
				return sender.sendError(httpCode, AppSettings.messages.at("warehouse-error-message"));
		}
	}
}
