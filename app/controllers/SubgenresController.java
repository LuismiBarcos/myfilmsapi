package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.Subgenre;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class SubgenresController extends Controller {
	
	@Inject
	private FormFactory formFactory; 	
	private final ISender sender;
	
	@Inject
	public SubgenresController(ISender sender) {
		this.sender = sender;
	}
		
	public Result retrieveSubgenres(Integer page) {
		List<Subgenre> subgenres;
		String name = request().getQueryString("name");
		if(name != null) {
			subgenres = Subgenre.searchByName(name);
			return sender.sendViews(subgenres, request());
		}
		subgenres = Subgenre.findPage(page).getList();
		return sender.sendViews(subgenres, request());
	}
	
	public Result retrieveSubgenre(Long subgenreId) {
		Subgenre subgenre = Subgenre.findById(subgenreId);
		return subgenre == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(subgenre, request());  
	}
	
	public Result createSubgenre() {
		Form<Subgenre> form = formFactory.form(Subgenre.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updateSubgenre() {
		Form<Subgenre> form = formFactory.form(Subgenre.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updateSubgenre();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deleteSubgenre(Long subgenreId) {
		return Subgenre.deleteSubgenre(subgenreId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
