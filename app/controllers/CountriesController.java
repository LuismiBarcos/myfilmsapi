package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.Country;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class CountriesController extends Controller {
	
	@Inject
	private FormFactory formFactory;
	private final ISender sender;
	
	@Inject
	public CountriesController(ISender sender) {
		this.sender = sender;
	}
		
	public Result retrieveCountries(Integer page) {
		List<Country> countries;
		String name = request().getQueryString("name");
		if(name != null) {
			countries = Country.searchByName(name);
			return sender.sendViews(countries, request());
		}
		countries = Country.findPage(page).getList();
		return sender.sendViews(countries, request());
	}
	
	public Result retrieveCountry(Long countryId) {
		Country country = Country.findById(countryId);
		return country == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(country, request());  
	}
	
	public Result createCountry() {
		Form<Country> form = formFactory.form(Country.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updateCountry() {
		Form<Country> form = formFactory.form(Country.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updateCountry();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deleteCountry(Long countryId) {
		return Country.deleteCountry(countryId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
