package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.Genre;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class GenresController extends Controller {
	
	@Inject
	private FormFactory formFactory; 	
	private final ISender sender;
	
	@Inject
	public GenresController(ISender sender) {
		this.sender = sender;
	}
		
	public Result retrieveGenres(Integer page) {
		List<Genre> genres;
		String name = request().getQueryString("name");
		if(name != null) {
			genres = Genre.searchByName(name);
			return sender.sendViews(genres, request());
		}
		genres = Genre.findPage(page).getList();
		return sender.sendViews(genres, request());
	}
	
	public Result retrieveGenre(Long genreId) {
		Genre genre = Genre.findById(genreId);
		return genre == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(genre, request()); 
	}
	
	public Result createGenre() {
		Form<Genre> form = formFactory.form(Genre.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updateGenre() {
		Form<Genre> form = formFactory.form(Genre.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updateGenre();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deleteGenre(Long genreId) {
		return Genre.deleteGenre(genreId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
