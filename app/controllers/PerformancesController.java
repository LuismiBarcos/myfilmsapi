package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.Performance;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class PerformancesController extends Controller {
	
	@Inject
	private FormFactory formFactory; 
	private final ISender sender;
	
	@Inject
	public PerformancesController(ISender sender) {
		this.sender = sender;
	}
		
	public Result retrievePerformances(Integer page) {
		List<Performance> performances;
		String name = request().getQueryString("name");
		if(name != null) {
			performances = Performance.searchByName(name);
			return sender.sendViews(performances, request());
		}
		performances = Performance.findPage(page).getList();
		return sender.sendViews(performances, request());
	}
	
	public Result retrievePerformance(Long performanceId) {
		Performance performance = Performance.findById(performanceId);
		return performance == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(performance, request());  
	}
	
	public Result createPerformance() {
		Form<Performance> form = formFactory.form(Performance.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updatePerformance() {
		Form<Performance> form = formFactory.form(Performance.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updatePerformance();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deletePerformance(Long performanceId) {
		return Performance.deletePerformance(performanceId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
