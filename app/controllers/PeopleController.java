package controllers;

import java.util.List;

import javax.inject.Inject;

import core.AppSettings;
import models.Person;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.ISender;
import validators.ActionAuthenticator;

@Security.Authenticated(ActionAuthenticator.class)
public class PeopleController extends Controller {
	
	@Inject
	private FormFactory formFactory; 	
	private final ISender sender;
	
	@Inject
	public PeopleController(ISender sender) {
		this.sender = sender;
	}
	
	public Result retrievePeople(Integer page) {
		List<Person> people;
		String name = request().getQueryString("name");
		if(name != null) {
			people = Person.searchByName(name);
			return sender.sendViews(people, request());
		}
		people = Person.findPage(page).getList();
		return sender.sendViews(people, request());
	}
	
	public Result retrievePerson(Long personId) {
		Person person = Person.findById(personId);
		return person == null 
				? Results.status(Http.Status.NOT_FOUND, AppSettings.messages.at("404-error-message")) 
				: sender.sendViews(person, request());  
	}
	
	public Result createPerson() {
		Form<Person> form = formFactory.form(Person.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().create();
		return httpCode == Http.Status.CREATED 
				? Results.created() 
				: sender.sendError(httpCode, AppSettings.messages.at("save-error"));
	}
	
	public Result updatePerson() {
		Form<Person> form = formFactory.form(Person.class).bindFromRequest(request());
		if(form.hasErrors()) {
			return Results.status(Http.Status.BAD_REQUEST, form.errorsAsJson());
		}
		Integer httpCode = form.get().updatePerson();
		return httpCode == Http.Status.OK 
				? ok() 
				: sender.sendError(httpCode, AppSettings.messages.at("update-error"));
	}
	
	public Result deletePerson(Long personId) {
		return Person.deletePerson(personId) 
				? ok() 
				: Results.status(Http.Status.INTERNAL_SERVER_ERROR, AppSettings.messages.at("delete-error"));
	}
}
