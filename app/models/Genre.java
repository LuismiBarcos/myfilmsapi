package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import core.AppSettings;
import io.ebean.*;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.mvc.Http;

@Entity
public class Genre extends Model{
	
	public static final Finder<Long,Genre> find = new Finder<>(Genre.class);
	
	@Id
	private Long id;
	@Required(message="required")
	@MinLength(AppSettings.MIN_CHARS_NAMES)
	private String name;
	@JsonIgnore
	@Null(message="not-necessary")
	@ManyToMany(mappedBy = "genres")
	private List<Movie> movie = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<Movie> getMovie() {
		return movie;
	}

	public void setMovie(List<Movie> movie) {
		this.movie = movie;
	}

	public static Genre findById(Long id) {
		if(id == null) {
			return null;
		}
		return find.byId(id);
	}
	
	public static List<Genre> searchByName(String name) {
		return find.query()
				.where()
					.contains("name", name)
				.findList();
	}
	
	public static PagedList<Genre> findPage(Integer page) {
		return find.query()
				.setMaxRows(AppSettings.MAX_ROWS)
				.setFirstRow(AppSettings.MAX_ROWS*page)
				.findPagedList();
	}
	
	public static boolean exists(Long id) {
		return Genre.findById(id) != null ? true : false; 
	}
	
	public static boolean deleteGenre(Long genreId) {
		Genre genre = Genre.findById(genreId);
		return genre == null ? true : genre.delete();
	}
	
	public Integer create() {
		if(id == null) {
			return saveGenre(this);
		}
		return Genre.exists(id) ? Http.Status.CONFLICT : saveGenre(this);
	}
	
	public Integer updateGenre() {
		return Genre.exists(id) ? updateGenre(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer saveGenre(Genre genre) {
		try {
			genre.save();
			return Http.Status.CREATED;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private Integer updateGenre(Genre genre) {
		try {
			genre.update();
			return Http.Status.OK;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
}
