package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import core.AppSettings;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.data.validation.Constraints.ValidateWith;
import play.mvc.Http;
import play.data.validation.ValidationError;
import validators.GenresValidator;
import validators.ProducersValidator;
import validators.SubgenresValidator;
import validators.TeamMembersValidator;

@Entity
public class Movie extends Model{
	
	private static final Integer MIN_YEAR = 1895; 	// First movie show
	private static final Integer MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR); 	// For movie premieres
	private static final Integer MOVIE_MAX_ROWS = 15;
	public static final Finder<Long,Movie> find = new Finder<>(Movie.class);
	
	@Id
	private Long id;
	
	@Required(message="required")
	@MinLength(1)
	private String title;
	
	@Required(message="required")
	@MinLength(1)
	private String originalTitle;
	
	private int year;
	
	@MinLength(1)
	private String duration;
	
	@Required(message="required")
	@ManyToOne
	private Country country;
	/*
	 * Info: https://stackoverflow.com/questions/16159978/playframework-2-java-model-string-field-length-annotations
	 */
	@Column(columnDefinition = "varchar(15000)")
	private String synopsis;
	
	@Column(columnDefinition = "varchar(5000)")
	private String awards;
	private String coverUrl;	
	
	@Required(message="required")
	@ValidateWith(TeamMembersValidator.class)
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "movie")
	private List<TeamMember> teamMembers = new ArrayList<>();
	
	@Required(message="required")
	@ValidateWith(ProducersValidator.class)
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Producer> producers = new ArrayList<>();
	
	@Required(message="required")
	@ValidateWith(GenresValidator.class)
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Genre> genres = new ArrayList<>();
	
	@ManyToMany(cascade=CascadeType.ALL)
	@ValidateWith(SubgenresValidator.class)
	private List<Subgenre> subgenres = new ArrayList<>();
	
	@JsonIgnore
	@Null(message="not-necessary")
	@OneToMany(mappedBy = "movie")
	private List<Warehouse> movieWarehouse = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getOriginalTitle() {
		return originalTitle;
	}
	
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public Country getCountry() {
		return country;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public String getSynopsis() {
		return synopsis;
	}
	
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	
	public String getAwards() {
		return awards;
	}
	
	public void setAwards(String awards) {
		this.awards = awards;
	}
	
	public String getCoverUrl() {
		return coverUrl;
	}
	
	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	public List<TeamMember> getTeamMembers() {
		return teamMembers;
	}

	public void setTeamMembers(List<TeamMember> teamMembers) {
		this.teamMembers = teamMembers;
	}

	public List<Producer> getProducers() {
		return producers;
	}

	public void setProducers(List<Producer> producers) {
		this.producers = producers;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public List<Subgenre> getSubgenres() {
		return subgenres;
	}

	public void setSubgenres(List<Subgenre> subgenres) {
		this.subgenres = subgenres;
	}

	public List<Warehouse> getMovieWarehouse() {
		return movieWarehouse;
	}

	public void setMovieWarehouse(List<Warehouse> movieWarehouse) {
		this.movieWarehouse = movieWarehouse;
	}
	
	public List<ValidationError> validate() {
    	List<ValidationError> errors = new ArrayList<ValidationError>();
    	if(year < MIN_YEAR || year > MAX_YEAR) {
    		errors.add(new ValidationError("year", AppSettings.messages.at("year-error")));
    	}
    	return errors.isEmpty() ? null : errors;
	}
	
	public static PagedList<Movie> findPage(Integer page){
		return find.query()
				.setMaxRows(MOVIE_MAX_ROWS)
				.setFirstRow(MOVIE_MAX_ROWS*page)
				.findPagedList();
	}

	public static Movie findById(Long id) {
		if(id == null) {
			return null;
		}
		return find.byId(id);
	}

	public static PagedList<Movie> searchByFilters(String title, Integer year, Integer page) {
		if(year == 0) {
			return find.query()
					.select("teammembers.name, title")
					.where()
						.contains("title", title)
					.setMaxRows(MOVIE_MAX_ROWS)
					.setFirstRow(MOVIE_MAX_ROWS*page)
					.findPagedList();
		}
		return find.query()
				.select("teamMember.name, title")
				.where()
					.contains("title", title)
					.eq("year", year)
				.setMaxRows(MOVIE_MAX_ROWS)
				.setFirstRow(MOVIE_MAX_ROWS*page)
				.findPagedList();
	}
	
	public static boolean exists(Long id) {
		return Movie.findById(id) != null ? true : false; 
	}
	
	public static boolean deleteMovie(Long movieId) {
		Movie movie = Movie.findById(movieId);
		return movie == null ? true : movie.delete();
	}
	
	public Integer create() {
		if(id == null) {
			return saveMovie(this);
		}
		return Movie.exists(id) ? Http.Status.CONFLICT : saveMovie(this);
	}
	
	public Integer updateMovie() {
		return Movie.exists(id) ? updateMovie(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer updateMovie(Movie movie) {
		if(checkDataExistence(movie)) {
			try{
				movie.update();
				return Http.Status.OK;
			}catch (Exception ex) {
				return Http.Status.INTERNAL_SERVER_ERROR;
			}
		}
		return Http.Status.NOT_FOUND;
	}
	
	private Integer saveMovie(Movie movie) {
		if(checkDataExistence(movie)) {
			try {
				movie.save();
				return Http.Status.CREATED;
			} catch(Exception ex) {
				return Http.Status.INTERNAL_SERVER_ERROR;
			}
		}
		return Http.Status.NOT_FOUND;
	}
    
    private boolean checkDataExistence(Movie movie) {
    	if(!existsGenres(movie.getGenres())) {    		
    		return false;
    	}
    	if(!existsProducers(movie.getProducers())) {
    		return false;
    	}
    	if(!existsSubgenres(movie.getSubgenres())) {
    		return false;
    	}    	
    	return true;
    }

    private boolean existsGenres(List<Genre> genres) {
		for (Genre genre : genres) {
			if(!Genre.exists(genre.getId())) {
				return false;
			}
		}
		return true;
	}

	private boolean existsProducers(List<Producer> producers) {
		for (Producer producer : producers) {
			if(!Producer.exists(producer.getId())) {
				return false;
			}
		}
		return true;
	}

	private boolean existsSubgenres(List<Subgenre> subgenres) {
		if(subgenres == null) {
			return true;	// Subgenre is not required
		}
		for (Subgenre subgenre : subgenres) {
			if(!Subgenre.exists(subgenre.getId())) {
				return false;
			}
		}
		return true;
	}
}
