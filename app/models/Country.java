package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import core.AppSettings;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.mvc.Http;;

@Entity
public class Country extends Model{
	
	public static final Finder<Long,Country> find = new Finder<>(Country.class);
	
	@Id
	private Long id;
	@Required(message="required")
	@MinLength(AppSettings.MIN_CHARS_NAMES)
	private String name;
	@JsonIgnore
	@Null(message="not-necessary")
	@OneToMany(cascade=CascadeType.ALL, mappedBy="country")
	private List<Movie> movies = new ArrayList<Movie>();
		
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}
	
	
	public static Country findById(Long id) {
		if(id == null) {
			return null;
		}
		return find.byId(id);
	}
	
	public static List<Country> searchByName(String name) {
		return find.query()
				.where()
					.contains("name", name)
				.findList();
	}
	
	public static PagedList<Country> findPage(Integer page) {
		return find.query()
				.setMaxRows(AppSettings.MAX_ROWS)
				.setFirstRow(AppSettings.MAX_ROWS*page)
				.findPagedList();
	}
	
	public static boolean deleteCountry(Long countryId) {
		Country country = Country.findById(countryId);
		return country == null ? true : country.delete();
	}

	public Integer create() {
		if(id == null) {
			return saveCountry(this);
		}
		return exists(id) ? Http.Status.CONFLICT : saveCountry(this);
	}
	
	public Integer updateCountry() {
		return exists(id) ? updateCountry(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer saveCountry(Country country) {
		try {
			country.save();
			return Http.Status.CREATED;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private Integer updateCountry(Country country) {
		try {
			country.update();
			return Http.Status.OK;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private boolean exists(Long id) {
		return Country.findById(id) != null ? true : false; 
	}
}
