package models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.ebean.Model;

@Table(name="TeamMember")
@Entity
public class TeamMember extends Model {
	
	@Id
	private Long id;
	@JsonBackReference
	@ManyToOne
	private Movie movie;
	@ManyToOne
	private Person person;
	@ManyToOne
	private Performance performance;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Movie getMovie() {
		return movie;
	}
	
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	public Person getPerson() {
		return person;
	}
	
	public void setPerson(Person person) {
		this.person = person;
	}
	
	public Performance getPerformance() {
		return performance;
	}
	
	public void setPerformance(Performance performance) {
		this.performance = performance;
	}	
}
