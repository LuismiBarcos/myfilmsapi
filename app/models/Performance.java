package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import core.AppSettings;
import io.ebean.*;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.mvc.Http;

@Entity
public class Performance extends Model{
	
	public static final Finder<Long,Performance> find = new Finder<>(Performance.class);
	
	@Id
	private Long id;
	@Required(message="required")
	@MinLength(AppSettings.MIN_CHARS_NAMES)
	private String name;
	@JsonIgnore
	@Null(message="not-necessary")
	@OneToMany(mappedBy = "performance")
	private List<TeamMember> teamMembers = new ArrayList<>(); 
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<TeamMember> getTeamMembers() {
		return teamMembers;
	}

	public void setTeam(List<TeamMember> teamMembers) {
		this.teamMembers = teamMembers;
	}

	public static Performance findById(Long id) {
		if(id == null) {
			return null;
		}
		return find.byId(id);
	}
	
	public static List<Performance> searchByName(String name) {
		return find.query()
				.where()
					.contains("name", name)
				.findList();
	}
	
	public static PagedList<Performance> findPage(Integer page) {
		return find.query()
				.setMaxRows(AppSettings.MAX_ROWS)
				.setFirstRow(AppSettings.MAX_ROWS*page)
				.findPagedList();
	}
	
	public static boolean deletePerformance(Long performanceId) {
		Performance performance = Performance.findById(performanceId);
		return performance == null ? true : performance.delete();
	}

	public Integer create() {
		if(id == null) {
			return savePerformance(this);
		}
		return exists(id) ? Http.Status.CONFLICT : savePerformance(this);
	}
	
	public Integer updatePerformance() {
		return exists(id) ? updatePerformance(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer savePerformance(Performance performance) {
		try {
			performance.save();
			return Http.Status.CREATED;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private Integer updatePerformance(Performance performance) {
		try {
			performance.update();
			return Http.Status.OK;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private boolean exists(Long id) {
		return Performance.findById(id) != null ? true : false; 
	}
}
