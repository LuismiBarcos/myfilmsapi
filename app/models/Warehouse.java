package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import core.AppSettings;
import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.Required;
import play.mvc.Http;

@Entity
public class Warehouse extends Model{
	
	public static final Finder<Long,Warehouse> find = new Finder<>(Warehouse.class);
	
	@Id
	private Long id;
	@Required(message="required")
	@ManyToOne
	private Movie movie;
	@Required(message="required")
	@ManyToOne
	private User user;
	private boolean favorite = false;
	private boolean viewed = false;
	private boolean pending = false;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Movie getMovie() {
		return movie;
	}
	
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isFavorite() {
		return favorite;
	}
	
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
	
	public boolean isViewed() {
		return viewed;
	}
	
	public void setViewed(boolean viewed) {
		this.viewed = viewed;
	}
	
	public boolean isPending() {
		return pending;
	}
	
	public void setPending(boolean pending) {
		this.pending = pending;
	}
	
	public static Warehouse findById(Long id) {
		return id == null ? null : find.byId(id);
	}
	
	public List<ValidationError> validate() {
    	List<ValidationError> errors = new ArrayList<ValidationError>();
    	if(validateMovie(movie) != null) {
    		errors.add(validateMovie(movie));
    	}
    	if(validateUser(user) != null) {
    		errors.add(validateUser(user));
    	}
    	return errors.isEmpty() ? null : errors;
	}
	
	private ValidationError validateMovie(Movie movie){
		if(movie == null) {
    		return new ValidationError("movie", AppSettings.messages.at("required"));
    	}
    	if(movie.getId() == null) {
    		return new ValidationError("movie", AppSettings.messages.at("warehouse-validate-error"));
    	}
    	return null;
	}
	
	private ValidationError validateUser(User user){
		if(user == null) {
    		return new ValidationError("user", AppSettings.messages.at("required"));
    	}
    	if(user.getId() == null) {
    		return new ValidationError("user", AppSettings.messages.at("warehouse-validate-error"));
    	}
    	return null;
	}
	
	public static Warehouse findByMovieAndUser(Long movieId, Long userId) {
		if(movieId == null || userId == null) {
			return null;
		}
		return find.query()
				.where()
					.eq("movie_id", movieId)
					.eq("user_id", userId)
				.findOne();
	}
	
	public Integer setWarehouse() {
		return exists(movie.getId(), user.getId()) ? updateOrDeleteWarehouse(this) : saveWarehouse(this);
	}
	
	private Integer updateOrDeleteWarehouse(Warehouse warehouse) {
		if(shouldRemove(warehouse)) {
			return removeWarehouse(warehouse);
		}
		return updateWarehouse(warehouse);
	}
	
	private Integer saveWarehouse(Warehouse warehouse) {
		if(shouldRemove(warehouse)) {
			return Http.Status.OK;
		}
		if(!Movie.exists(warehouse.getMovie().getId()) || !User.exists(warehouse.getMovie().getId())) {
			return Http.Status.NOT_FOUND;
		}
		try {
			warehouse.save();
			return Http.Status.CREATED;
		} catch (Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
		
	}
	
	private Integer updateWarehouse(Warehouse wh) {
		Warehouse warehouse = Warehouse.findByMovieAndUser(wh.getMovie().getId(), wh.getUser().getId());
		warehouse.setPending(wh.isPending());
		warehouse.setFavorite(wh.isFavorite());
		warehouse.setViewed(wh.isViewed());
		try {
			warehouse.update();
			return Http.Status.OK;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private Integer removeWarehouse(Warehouse warehouse) {
		warehouse = Warehouse.findByMovieAndUser(warehouse.getMovie().getId(), warehouse.getUser().getId());
		return warehouse.delete() ? Http.Status.OK : Http.Status.INTERNAL_SERVER_ERROR;
	}
	
	private boolean exists(Long movieId, Long userId) {
		return Warehouse.findByMovieAndUser(movieId, userId) == null ? false : true;
	}
	
	private boolean shouldRemove(Warehouse warehouse) {
		return warehouse.favorite == false && warehouse.pending == false && warehouse.viewed == false;
	}
}
