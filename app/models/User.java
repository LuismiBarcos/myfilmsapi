package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import core.AppSettings;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.mvc.Http;

@Entity
public class User extends Model{
	
	public static final Finder<Long,User> find = new Finder<>(User.class);
	
	@Id
	private Long id;
	
	@Required(message="required")
	private String name;
	
	@Required(message="required")
	@Email(message="email-error")
	private String email;
	
	@JsonIgnore
	@Required(message="required")
	private String pass;
	
	@Null(message="not-necessary")
	@OneToOne(cascade=CascadeType.ALL)
	private ApiKey apiKey;
	
	@Null(message="not-necessary")
	@OneToMany(mappedBy = "user")
	private List<Warehouse> movieWarehouse = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public ApiKey getApiKey() {
		return apiKey;
	}
	
	public void setApiKey(ApiKey apiKey) {
		this.apiKey = apiKey;
	}

	public List<Warehouse> getMovieWarehouse() {
		return movieWarehouse;
	}

	public void setMovieWarehouse(List<Warehouse> movieWarehouse) {
		this.movieWarehouse = movieWarehouse;
	}
	
	
	public List<ValidationError> validate() {
    	List<ValidationError> errors = new ArrayList<ValidationError>();
    	if(pass == null) {
    		errors.add(new ValidationError("pass", AppSettings.messages.at("required")));
    		return errors;
    	}
    	if(pass.length() < 8) {
    		errors.add(new ValidationError("pass", AppSettings.messages.at("pass-error")));
    	}
    	return errors.isEmpty() ? null : errors;
	}
	
	public static PagedList<User> findPage(Integer page) {
		return find.query()
				.setMaxRows(AppSettings.MAX_ROWS)
				.setFirstRow(AppSettings.MAX_ROWS*page)
				.findPagedList();
	}

	public static User findById(Long userId) {
		if(userId == null) {
			return null;
		}
		return find.byId(userId);
	}
	
	public static User findByUsername(String username) {
		return find.query()
				.where()
					.eq("name", username)
				.findOne();
	}
	
	public static User findByEmail(String email) {
		return find.query()
				.where()
					.eq("email", email)
				.findOne();
	}
	
	public static List<User> searchByUsername(String username) {
		return find.query()
				.where()
					.contains("name", username)
				.findList();
	}
	
	public static String findNameByApiKey(Long apiKeyId) {
		return find.query()
				.where()
					.eq("api_key_id", apiKeyId)
				.findOne()
				.getName();
	}
	
	public static boolean exists(Long userId) {
		return User.findById(userId) == null ? false : true;
	}
	
	public static boolean deleteUser(Long userId) {
		User user = User.findById(userId);
		return user == null ? true : user.delete();
	}

	public Integer create() {
		if(id == null) {
			return saveUser(this);
		}
		return User.exists(id) ? Http.Status.CONFLICT : saveUser(this);
	}
	
	public Integer updateUser() {
		return User.exists(id) ? updateUser(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer saveUser(User user) {
//		if(User.findByUsername(user.getName()) == null && User.findByEmail(user.getEmail()) == null) {
		if(!exists(user.getName(), user.getEmail())) {
			user.setPass(createSha2Hash(user.getPass()));
			user.setApiKey(createApiKey(user));
			try {
				user.save();
				return Http.Status.CREATED;
			} catch(Exception ex) {
				return Http.Status.INTERNAL_SERVER_ERROR;
			}
		}
		return Http.Status.CONFLICT;		
	}
	
	private Integer updateUser(User user) {
		if(!exists(user.getName(), user.getEmail())) {
			try {
				user.setPass(createSha2Hash(user.getPass()));
				user.update();
				return Http.Status.OK;
			} catch(Exception ex) {
				return Http.Status.INTERNAL_SERVER_ERROR;
			}
		}
		return Http.Status.CONFLICT;
	}	
	
	private boolean exists(String username, String email) {
		if(User.findByUsername(username) == null && User.findByEmail(email) == null) {
			return false;
		}
		return true;
	}
	
	private ApiKey createApiKey(User user) {
		String uniqueText = user.getName() + user.getPass();
		ApiKey apiKey = new ApiKey();
		apiKey.setApiKey(createSha2Hash(uniqueText));
		return apiKey;
	}
	
	/*
	 * How to create a SHA-2 from text:
	 * https://stackoverflow.com/questions/5531455/how-to-hash-some-string-with-sha256-in-java
	 */
	private String createSha2Hash(String text) {
		return org.apache.commons.codec.digest.DigestUtils.sha256Hex(text);
	}
}
