package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import akka.http.javadsl.model.ws.Message;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.mvc.Http;
import core.AppSettings;

@Entity
public class Person extends Model{
	
	public static final Finder<Long,Person> find = new Finder<>(Person.class);
	
	@Id
	private Long id;
	@Required(message="required")
	@MinLength(AppSettings.MIN_CHARS_NAMES)
	private String name;	
	@JsonIgnore
	@Null(message="not-necessary")
	@OneToMany(mappedBy = "person")
	private List<TeamMember> teamMembers = new ArrayList<>();	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<TeamMember> getTeamMembers() {
		return teamMembers;
	}

	public void setTeamMembers(List<TeamMember> teamMembers) {
		this.teamMembers = teamMembers;
	}

	public static Person findById(Long id) {
		if(id == null) {
			return null;
		}
		return find.byId(id);
	}
	
	public static List<Person> searchByName(String name) {
		return find.query()
				.where()
					.contains("name", name)
				.findList();
	}
	
	public static PagedList<Person> findPage(Integer page) {
		return find.query()
				.setMaxRows(AppSettings.MAX_ROWS)
				.setFirstRow(AppSettings.MAX_ROWS*page)
				.findPagedList();
	}
	
	public static boolean deletePerson(Long personId) {
		Person person = Person.findById(personId);
		return person == null ? true : person.delete();
	}

	public Integer create() {
		if(id == null) {
			return savePerson(this);
		}
		return exists(id) ? Http.Status.CONFLICT : savePerson(this);
	}
	
	public Integer updatePerson() {
		return exists(id) ? updatePerson(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer savePerson(Person person) {
		try {
			person.save();
			return Http.Status.CREATED;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private Integer updatePerson(Person person) {
		try {
			person.update();
			return Http.Status.OK;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private boolean exists(Long id) {
		return Person.findById(id) != null ? true : false; 
	}
}
