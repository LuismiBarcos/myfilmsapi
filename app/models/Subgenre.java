package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import core.AppSettings;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.mvc.Http;

@Entity
public class Subgenre extends Model{
	
	public static final Finder<Long,Subgenre> find = new Finder<>(Subgenre.class);
	
	@Id
	private Long id;
	@Required(message="required")
	@MinLength(AppSettings.MIN_CHARS_NAMES)
	private String name;
	@JsonIgnore
	@Null(message="not-necessary")
	@ManyToMany(mappedBy = "subgenres")
	private List<Movie> movie = new ArrayList<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Movie> getMovie() {
		return movie;
	}

	public void setMovie(List<Movie> movie) {
		this.movie = movie;
	}
	
	public static Subgenre findById(Long id) {
		if(id == null) {
			return null;
		}
		return find.byId(id);
	}
	
	public static List<Subgenre> searchByName(String name) {
		return find.query()
				.where()
					.contains("name", name)
				.findList();
	}
	
	public static PagedList<Subgenre> findPage(Integer page) {
		return find.query()
				.setMaxRows(AppSettings.MAX_ROWS)
				.setFirstRow(AppSettings.MAX_ROWS*page)
				.findPagedList();
	}
	
	public static boolean exists(Long id) {
		return Subgenre.findById(id) != null ? true : false; 
	}	
	
	public static boolean deleteSubgenre(Long subgenreId) {
		Subgenre subgenre = Subgenre.findById(subgenreId);
		return subgenre == null ? true : subgenre.delete();
	}

	public Integer create() {
		if(id == null) {
			return saveSubgenre(this);
		}
		return Subgenre.exists(id) ? Http.Status.CONFLICT : saveSubgenre(this);
	}
	
	public Integer updateSubgenre() {
		return Subgenre.exists(id) ? updateSubgenre(this) : Http.Status.NOT_FOUND;
	}
	
	private Integer saveSubgenre(Subgenre subgenre) {
		try {
			subgenre.save();
			return Http.Status.CREATED;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
	
	private Integer updateSubgenre(Subgenre subgenre) {
		try {
			subgenre.update();
			return Http.Status.OK;
		} catch(Exception ex) {
			return Http.Status.INTERNAL_SERVER_ERROR;
		}
	}
}
