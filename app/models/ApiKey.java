package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class ApiKey extends Model{
	
	private static final Finder<Long,ApiKey> find = new Finder<>(ApiKey.class);
	
	@Id
	private Long Id;
	
	@JsonBackReference
	@OneToOne(mappedBy="apiKey")
	private User user;
	private String apiKey;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public static ApiKey findByHash(String hash) {
		if(hash == null) {
			return null;
		}
		return find.query()
				.where()
					.eq("api_key", hash)
				.findOne();
	}
}
