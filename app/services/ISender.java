package services;

import java.util.List;

import models.*;
import play.mvc.Http.Request;
import play.mvc.Result;

public interface ISender {
	
	public Result sendError(Integer code, String message);
	
	public Result sendViews(List<?> value, Request request);
	
	public Result sendViews(Genre value, Request request);
	
	public Result sendViews(Subgenre value, Request request);
	
	public Result sendViews(Producer value, Request request);
	
	public Result sendViews(Performance value, Request request);
	
	public Result sendViews(Person value, Request request);
	
	public Result sendViews(Country value, Request request);
	
	public Result sendViews(User value, Request request);
	
	public Result sendViews(Movie value, Request request);
}
