package core;

import java.util.List;

import models.*;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.libs.Json;
import play.libs.XML;
import play.mvc.Result;
import play.mvc.Results;
import services.ISender;

public class Sender implements ISender{

	private final Integer FIRST_ELEMENT = 0;
	
	@Override
	public Result sendError(Integer code, String message) {
		return Results.status(code, message);
	}
	
	@Override
	public Result sendViews(List value, Request request) {
		if(request.accepts("application/xml")) {
			return value.size() > 0
					? returnXML(value)
					: Results.status(Http.Status.NO_CONTENT);
		} else if(request.accepts("application/json")) {
			return value.size() > 0
					? Results.ok(Json.toJson(value))
					: Results.status(Http.Status.NO_CONTENT);
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}
	
	@Override
	public Result sendViews(Genre value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.genre.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}

	@Override
	public Result sendViews(Subgenre value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.subgenre.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}
	
	@Override
	public Result sendViews(Producer value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.producer.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}

	@Override
	public Result sendViews(Performance value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.performance.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}

	@Override
	public Result sendViews(Person value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.person.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}

	@Override
	public Result sendViews(Country value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.country.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}
	
	@Override
	public Result sendViews(User value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.usersViews.user.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}

	@Override
	public Result sendViews(Movie value, Request request) {
		if(request.accepts("application/xml")) {
			return Results.ok(views.xml.moviesViews.movie.render(value));
		} else if(request.accepts("application/json")) {
			return Results.ok(Json.toJson(value));
		} else {
			return Results.status(Http.Status.NOT_ACCEPTABLE, AppSettings.messages.at("not-acceptable-error"));
		}
	}	
	
	private Result returnXML(List value) {
		if(value.get(FIRST_ELEMENT) instanceof Genre) {
			return Results.ok(views.xml.moviesViews.genres.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof Subgenre) {
			return Results.ok(views.xml.moviesViews.subgenres.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof Producer) {
			return Results.ok(views.xml.moviesViews.producers.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof Performance) {
			return Results.ok(views.xml.moviesViews.performances.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof Person) {
			return Results.ok(views.xml.moviesViews.people.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof Country) {
			return Results.ok(views.xml.moviesViews.countries.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof User) {
			return Results.ok(views.xml.usersViews.users.render(value));
		}
		if(value.get(FIRST_ELEMENT) instanceof Movie) {
			return Results.ok(views.xml.moviesViews.movies.render(value));
		}
		return sendError(Http.Status.NOT_IMPLEMENTED, AppSettings.messages.at("not-available-xml-view"));
	}	
}
