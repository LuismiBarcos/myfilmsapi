package core;

import play.i18n.Messages;
import play.mvc.Http;

public class AppSettings {
	public final static int MIN_CHARS_NAMES = 3;
	public final static int MAX_ROWS = 30;
	public final static Messages messages = Http.Context.current().messages();
}
